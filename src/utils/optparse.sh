#!/usr/bin/env sh


_CONF_FILE="${XDG_CONFIG_HOME:-$HOME/.config}/mss/conf.sh"
if [ -e "${_CONF_FILE}" ]; then
	#shellcheck disable=SC1090
	. "${_CONF_FILE}"
fi

# shellcheck source=./src/HTTP/wget.sh
. "./src/HTTP/${HTTP_CLIENT:-wget}.sh" || { echo "$HTTP_CLIENT client conf not found" && exit 1; }
