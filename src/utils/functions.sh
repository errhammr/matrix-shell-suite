#!/usr/bin/env sh

#  Common utility functions for Matrix Shell Suite
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

exists() {
	if ! command -v "$1" >/dev/null 2>&1; then
		>&2 echo "Command $1 not found in \$PATH"
		return 1
	fi
	return 0
}

# Method taken from https://stackoverflow.com/a/28393320
read_secret() {
	# Disable echo.
	stty -echo

	# Set up trap to ensure echo is enabled before exiting if the script
	# is terminated while echo is disabled.
	trap 'stty echo' EXIT

	# Read secret.
	read -r "$@"

	# Enable echo. We implicitly assume here that echo was enabled in the shell
	# before calling this function. It can be handled, but I'd rather not
	# unless absolutely necessary
	stty echo
	trap - EXIT

	# Print a newline because the newline entered by the user after
	# entering the passcode is not echoed. This ensures that the
	# next line of output begins at a new line.
	echo
}


# This function accepts a servername according to the matrix spec and extracts
# the hostname.
_get_hostname() {
	servername="$1"
	port="${servername##*:}"

	# If no ":" exists in the servername, then the entire string is the hostname
	if [ -z "$port" ]; then
		echo "$servername"
		return
	fi

	if echo "$port" | grep -q "]"; then
		# If a colon exists, and the shortest substr at the end upto the last :
		# contains a ']' then, the servername is an IPv6 address with no
		# explicit port
		echo "$servername"
	else
		echo "${servername%:*}"
	fi
}

test_server() {
	output="$(HGET "$1/_matrix/client/versions")" || return 1
	supported_versions="$(echo "$output" | jq '.versions[]' | xargs)" || return 2
	(echo "$supported_versions" | grep -q 'r0.4.0') || return 3
}

prompt_for_HS_URL() {
	>&2 printf "Automatic detection of Homeserver failed\n"
	>&2 printf "Please enter Homeserver URL: "
	read -r HSU
	echo "$HSU"
}

get_username() {
	matrix_id="$1"
	username="${matrix_id%%:*}"
	username="${username#@}"
	echo "$username"
}

get_homeserver_url2() {
	# Step 1: Extract the server name from the user's Matrix ID by splitting
	# the Matrix ID at the first colon.
	matrix_id="$1"
	server_name="${matrix_id#*:}"

	# Step 2: Extract the hostname from the server name.
	hostname="$(_get_hostname "$server_name")"

	# Step 3.1: Make a GET request to
	# https://hostname/.well-known/matrix/client.

	set +e
	wk_file="$(HGET "https://$hostname/.well-known/matrix/client")"
	res_code=$?
	set -e

	res_code="$(retcode_to_http "$res_code")"

	if [ "$res_code" = 'none' ]; then
		>&2 printf "Error Parsing Server Response.\n"
		return 1
	fi

	# Step 3.2: If the returned status code is 404, then IGNORE.
	# Step 3.3: If the returned status code is not 200, or the response body is
	# empty, then FAIL_PROMPT.
	# Step 3.4: Parse the response as a JSON object.
	# Step 3.5: Expect the base_url value from the m.homeserver property. If
	# this value is not provided, FAIL_PROMPT
	if [ "$res_code" -eq 404 ]; then
		if test_server "https://$hostname"; then
			echo "$hostname"
			return 0
		else
			server_url="$(prompt_for_HS_URL)"
		fi
	elif [ "$res_code" -ne 200 ] || [ -z "$wk_file" ]; then
		server_url="$(prompt_for_HS_URL)"
	else
		server_url="$(echo "$wk_file" | jq '."m.homeserver".base_url' | tr -d '"')" || prompt_for_HS_URL
		if [ -z "$server_url" ]; then
			server_url="$(prompt_for_HS_URL)"
		fi
	fi

	test_server "$server_url" || {
		ec=$?
		>&2 printf "Could not connect to the server. Reason: %s" $ec
		return 1
	}

	echo "$server_url"
}


get_homeserver_url() {
	url="$1"

	# Set the https:// prefix if necessary
	url=$(echo "$url" | sed 's#^http://#https://#g')
	url=$(echo "$url" | sed '/^https/! s#^#https://#g')

	# Set the port number (443) if not specified
	url=$(echo "$url" | sed '/:[0-9]*$/! s#$#:443#g')

	echo "$url"
}
