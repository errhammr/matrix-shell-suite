#!/usr/bin/env sh

exists() {
	if ! command -v "$1" >/dev/null 2>&1; then
		>&2 echo "Command $1 not found in \$PATH"
		return 1
	fi
	return 0
}

http_to_retcode() {
	case "$1" in
		200) echo 0;;
		404) echo 4;;
		*) echo 1;;
	esac
}

retcode_to_http() {
	case "$1" in
		0) echo "200";;
		4) echo "404";;
		1) echo "1" ;;
		*) echo "none";;
	esac
}
