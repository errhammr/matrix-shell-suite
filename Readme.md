# Matrix Shell Suite

Hi there!

This project is about connecting to the matrix chat network using shell scripts.


## Goals

* sending and receiving matrix chat messages
* keeping the scripts portable so they run on any POSIX compatible shell

## Requirements

In order to use this set of scripts, you need the following:

 * A POSIX compatible shell (`sh`, `dash`, `bash`, etc.)
 * [jq](https://github.com/stedolan/jq)

For End-to-End Encryption (E2EE), one can use [pantalaimon](https://github.com/matrix-org/pantalaimon) as a proxy.
Pantalaimon requires a modern Python interpretor and the olm library.
In the future, there will be easy support for using pantalaimon proxy.

## Structure

This repository is structured into multiple sub-directories:

 * bash-doxygen: This is actually a git submodule containing `bash-doxygen` a sed script for generating Doxygen based documentation out of Bash scripts
 * contrib: User contributed scripts
 * doc: Location where the Documentation is generated using `make doc`
 * images: Logos
 * src: Main scripts for use
   * src/HTTP: Contains abstractions for various HTTP clients
   * src/utils: Common utility functions
 * tests: Some tests for the repository
 * unit-tests: Unit tests for the scripts

## Help is appreciated

Help us improve the functionality and code quality of the scripts. Open an issue
if things don't work as expected. Spread the word.

You can find us in the matrix: [\#matrix-shell-suite:matrix.org](https://matrix.to/#/!qAVFqlBoxJSJCUdUSS:matrix.org?via=matrix.org&via=talk.go7box.xyz&via=t2bot.io)
