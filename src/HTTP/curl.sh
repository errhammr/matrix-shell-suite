#!/usr/bin/env sh

#  Curl Abstractions for Matrix Shell Suite
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

# shellcheck source=./src/utils/http_common.sh
. src/utils/http_common.sh

exists curl

if [ -n "${DEBUG_MATRIX:-}" ]; then
    DEBUG_PARAMS="--verbose"
else
    DEBUG_PARAMS=""
fi

CURL_PARAMS="--disable --location --suppress-connect-headers --silent ${DEBUG_PARAMS}"


__call_curl() {
	tmpfile=$(mktemp)

	#shellcheck disable=SC2086
	command curl $CURL_PARAMS "$@" -D "$tmpfile"

	ret=$(http_to_retcode "$(grep -e "HTTP/1.1" -e "HTTP/2" "$tmpfile" | tail -1 | awk '{print $2}')")
	rm "$tmpfile"
	return "$ret"
}

HGET() {
    URL="$1" && shift

    #shellcheck disable=SC2086
    __call_curl "$@" "$URL"
}

HGET_AUTH() {
    URL="$1" && shift
    auth_token="$1"

    HGET "$URL" --header "Authorization: Bearer $auth_token"
}

HPOST() {
    URL="$1" && shift
    DATA="$1" && shift

    #shellcheck disable=SC2086
    __call_curl --post301 --post302 --data-raw "$DATA" "$@" "$URL"
}

HPOST_AUTH() {
    URL="$1" && shift
    auth_token="$1" && shift
    DATA=$*

    HPOST "$URL" "$DATA" --header "Authorization: Bearer $auth_token"
}

HPUT() {
    URL="$1" && shift
    DATA="$1" && shift

    #shellcheck disable=SC2086
    __call_curl -X "PUT" --header "Content-Type: application/json" --data "$DATA" "$@" "$URL"
}

HPUT_AUTH() {
    URL="$1" && shift
    auth_token="$1" && shift
    DATA=$*

    HPUT "$URL" "$DATA" --header "Authorization: Bearer $auth_token"
}
